up:
	docker-compose up -d --force-recreate

logs:
	docker-compose logs -f

stop:
	docker-compose stop

shell:
	docker-compose exec php bash

node:
	docker-compose exec node bash

rm:
	docker-compose rm -f
